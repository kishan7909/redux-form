import React from "react";
import { Field, reduxForm, FieldArray } from "redux-form";
import { connect } from "react-redux";
import { load } from "../store/actions/loadData";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { isNull } from "util";

const data = {
  firstname: "Kishan",
  Lastname: "Patel",
  age: "23",
  anniversarydate: "2017-03-23",
  sex: "male",
  employed: true,
  favcolor: "Blue",
  bio: "Born to write amazing Redux code.",
  contactnumber: ["9825600441", "9824411001"]
};

const validate = values => {
  const errors = {};
  if (!values.firstname) {
    errors.firstname = "Required";
  } else if (values.firstname.length > 15) {
    errors.firstname = "Must be 15 characters or less";
  }

  if (!values.Lastname) {
    errors.Lastname = "Required";
  } else if (values.Lastname.length > 15) {
    errors.Lastname = "Must be 15 characters or less";
  }

  if (!values.age) {
    errors.age = "Required";
  } else if (isNaN(Number(values.age))) {
    errors.age = "Must be a number";
  } else if (Number(values.age) < 18) {
    errors.age = "Sorry, you must be at least 18 years old";
  }

  if (!values.anniversarydate) {
    errors.anniversarydate = "Required";
  }

  if (!values.sex) {
    errors.sex = "Required";
  } else if (isNull(values.sex)) {
    errors.sex = "Please gender type";
  }

  if (!values.favcolor) {
    errors.sex = "Required";
  } else if (isNull(values.favcolor)) {
    errors.sex = "Please select color";
  }

  console.log(errors);
};

const renderField = ({
  input,
  label,
  type,
  meta: { touched, error, warning }
}) => (
  <React.Fragment>
    <label>{label}</label>
    <div className="form-inline">
      <input
        {...input}
        placeholder={label}
        type={type}
        className="form-control col-md-9"
      />&nbsp;
      {touched &&
        ((error && (
          <span className="text-danger">
            <i className="mr-2">
              <FontAwesomeIcon icon="exclamation-circle" />
            </i>
            {error}
          </span>
        )) ||
          (warning && <span>{warning}</span>))}
    </div>
  </React.Fragment>
);

let ContactForm = props => {
  const { handleSubmit, pristine, submitting } = props;
  console.log(handleSubmit);
  const colors = [
    "Red",
    "Orange",
    "Yellow",
    "Green",
    "Blue",
    "Indigo",
    "Violet"
  ];

  const randercontactnumber = ({ fields }) => (
    <div className="form-inline">
      {fields.map((name, index) => (
        <div className="form-group mt-2" key={index + 1}>
          <Field
            name={name}
            type="text"
            component="input"
            className="form-control"
          />&nbsp;
          <button
            onClick={() => fields.splice(index, 1)}
            className="btn btn-danger"
          >
            -
          </button>
        </div>
      ))}
      &nbsp;
      <button onClick={() => fields.push()} className="btn btn-success mt-2">
        Add
      </button>
    </div>
  );

  return (
    <React.Fragment>
      <form onSubmit={handleSubmit}>
        <div className="col-md-6 offset-md-3">
          <div className="form-grroup">
            <button
              onClick={() => props.loaddata(data)}
              type="button"
              className="btn btn-info"
            >
              Load
            </button>
          </div>
          <div className="form-group">
            <Field
              label="Firstname"
              component={renderField}
              type="text"
              name="firstname"
              id="firstname"
              placeholder="FirstName"
            />
          </div>
          <div className="form-group">
            <Field
              label="Lastname"
              component={renderField}
              type="text"
              name="Lastname"
              id="Lastname"
              className="form-control"
              placeholder="LastName"
            />
          </div>
          <div className="form-group">
            <Field
              label="Age"
              component={renderField}
              type="number"
              name="age"
              id="age"
              className="form-control"
            />
          </div>
          <div className="form-group">
            <Field
              label="Anniversary Date"
              component={renderField}
              type="date"
              name="anniversarydate"
              id="anniversarydate"
              className="form-control"
            />
          </div>
          <div className="form-group">
            <label>Sex</label>
            <div className="form-inline">
              <div className="form-check">
                <Field
                  label="Sex"
                  component="input"
                  type="radio"
                  name="sex"
                  className="form-check-input"
                  value="male"
                  id="male"
                />{" "}
                <label htmlFor="male">Male</label>&nbsp;
              </div>
              <div className="form-check">
                <Field
                  component="input"
                  type="radio"
                  name="sex"
                  className="form-check-input"
                  value="female"
                  id="female"
                />{" "}
                <label htmlFor="female" className="form-check-label">
                  Female
                </label>
              </div>
            </div>
          </div>
          <div className="form-group">
            <label>FavoriteColor</label>
            <Field name="favcolor" component="select" className="form-control">
              <option value="">Select Color</option>
              {colors.map(color => (
                <option value={color} key={color}>
                  {color}
                </option>
              ))}
            </Field>
          </div>
          <div className="form-group form-check">
            <label htmlFor="employed">
              <Field
                name="employed"
                id="employed"
                component="input"
                type="checkbox"
                className="form-check-input"
              />
              Employed
            </label>
          </div>
          <div className="form-group">
            <label>Bio</label>
            <Field component="textarea" name="bio" className="form-control" />
          </div>

          <div className="form-group">
            <label>Contact No</label>

            <FieldArray
              component={randercontactnumber}
              name="contactnumber"
              className="form-control"
            />
          </div>

          <div className="form-grroup">
            <button
              type="submit"
              className="btn btn-info"
              disabled={pristine || submitting}
            >
              Submit
            </button>
          </div>
        </div>
      </form>
    </React.Fragment>
  );
};

ContactForm = reduxForm({
  form: "contact",
  validate
})(ContactForm);

const mapStateToProps = state => {
  return {
    initialValues: state.contactReducer.data
  };
};

const mapDispatchToProps = dispatch => {
  return {
    loaddata: data => {
      const action = load(data);
      dispatch(action);
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ContactForm);
