import React from "react";
import { connect } from "react-redux";
import { getFormValues } from "redux-form";
const DisplayFormData = props => {
  return (
    <div className="card card-body bg-light col-md-6 offset-md-3 mt-3">
      <pre className="text-success">
        {JSON.stringify(props.values, null, 2)}
      </pre>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    values: getFormValues("contact")(state)
  };
};

export default connect(mapStateToProps)(DisplayFormData);
