import React, { Component } from "react";
import ContactPage from "./component/ContactPage";
import "bootstrap/dist/css/bootstrap.css";
import "./libraries/fontawesom";

// import './App.css';

class App extends Component {
  render() {
    return (
      <div>
        <ContactPage />
      </div>
    );
  }
}

export default App;
