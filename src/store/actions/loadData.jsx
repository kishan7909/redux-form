export const load = data => {
  return {
    type: "LOAD",
    data
  };
};
