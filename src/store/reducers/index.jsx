import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";
import { contactReducer } from "./contactReducer";

console.log("rootReducerCall");

export const rootReducer = combineReducers({
  form: formReducer,
  contactReducer: contactReducer
});
