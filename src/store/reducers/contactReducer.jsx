const initState = {};

export const contactReducer = (state = initState, action) => {
  console.log("Contact Reducer", action);

  switch (action.type) {
    case "LOAD":
      return {
        data: action.data
      };
    default:
      return state;
  }
};
