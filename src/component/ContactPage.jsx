import React, { Component } from "react";
import ContactForm from "../containers/ContactForm";
import DisplayFormData from "../containers/DisplayFormData";

class ContactPage extends Component {
  state = {};
  submit = values => {
    // print the form values to the console
    console.log("Submit Click", values);
  };
  render() {
    return (
      <div className="container">
        <div className="card">
          <div className="card-header bg-info text-center text-white">
            <h2>Simple Form</h2>
          </div>
          <div className="card-body">
            <ContactForm onSubmit={this.submit} />

            <DisplayFormData />
          </div>
        </div>
      </div>
    );
  }
}

export default ContactPage;
